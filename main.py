from typing import Union

from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return "Hello bigdata phuong quynh"

@app.get("/hello")
def read_root():
    return "Hello bigdata"

@app.get("/hello_v2")
def read_root():
    return "Hello bigdata"

@app.get("/get_query")
def read_root():
    query = """Select * from `bigdata.test_v1`"""
    return {"Query": query}
@app.get("/items/{item_id}")
def read_item(item_id: int, value: int):
    query = f""" Select * from table where item_id = {item_id} and revenue = {value}"""
    return {"loai xe": "xe dap", "so banh": '2 banh'}