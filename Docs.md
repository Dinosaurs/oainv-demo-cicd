# Quy trình CICD

## Xuất phát điểm:

- Dev sẽ tạo ra các chương trình thông qua việc viết code. (Gitlab sẽ là nơi quản lý code)
  - Viết 1 application fast api đơn giản

- Sau khi đã viết các application thì cần buil thành các images để sử dụng run trên docker.
  - Cần viết docker file để build images.
  - Cần build images và run test trước ở local đảm bảo run thành công

- Push code lên gitlab
- Tạo file .gitlab-ci để bắt đầu quá trình CI
  -  Define các bước mà CI cần làm trong file gitlab-ci
  -  B1: Gitlab CI nó sẽ build images dựa trên project trên gitlab. Nơi lưu trữ images sẽ là gitlab VM
  -  B2: push images lên dockerhub
  -  Trong quá trình này khi c